<?php
	Class Lesson_mdl extends CI_Model {
		private $_table = 'lesson';
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add($obj) {
			$lessons = $this->_conn->retrieve($this->_table, array('lesson_id' => $obj['course_id']), '', true);
			$latest_sequence = ($lessons != NULL) ? count($lessons) : 0;
			$obj['sequence'] = $latest_sequence+1;
			return $this->_conn->save($obj, $this->_table);
		}

		public function get($obj='') {
			return $this->_conn->retrieve($this->_table, $obj, '', true);
		}

		public function edit($obj, $lesson_id) {
			return $this->_conn->edit($obj, $this->_table, array('lesson_id' => $lesson_id));
		}
	}
?>