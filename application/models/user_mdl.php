<?php
	Class User_mdl extends CI_Model {
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->load->model('course_mdl', 'course');
			$this->load->model('lesson_mdl', 'lesson');
			$this->_conn = new Conn();
		}

		public function detailed_course() {
			$courses = $this->course->get();
			for($i = 0; $i < count($courses); $i++) {
				$courses[$i]->lessons = $this->lesson->get(array('course_id' => $courses[$i]->course_id));
			}
			return $courses;
		}
	}
?>