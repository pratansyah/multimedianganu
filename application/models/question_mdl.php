<?php
	Class Question_mdl extends CI_Model {
		private $_table = 'multiple_question';
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add($obj) {
			return $this->_conn->save($obj, $this->_table, true);
		}

		public function get($obj='') {
			return $this->_conn->retrieve($this->_table, $obj);
		}

		public function edit($obj) {
			return $this->_conn->edit($obj['update'], $this->_table, $obj['where']);
		}
	}
?>