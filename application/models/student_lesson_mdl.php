<?php
	Class Student_lesson_mdl extends CI_Model {
		private $_table = 'student_lesson';
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add($obj) {
			return ($this->_conn->retrieve($this->_table, $obj)) ? '' : $this->_conn->save($obj, $this->_table);
		}

		public function get($obj='') {
			return $this->_conn->retrieve($this->_table, $obj, true);
		}

		public function latest_lesson($student_id, $course_id) {
			$query = "	SELECT 	max(l.sequence) as latest
						FROM 	lesson l, student_lesson sl
						WHERE 	l.lesson_id=sl.lesson_id AND
								sl.student_id=$student_id AND
								l.course_id=$course_id";
			return $this->_conn->nativeQuery($query);
		}
	}
?>