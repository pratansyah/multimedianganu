<?php
	Class Score_mdl extends CI_Model {
		private $_table = 'score';
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add($obj) {
			return $this->_conn->save($obj, $this->_table);
		}

		public function get($obj='') {
			return $this->_conn->retrieve($this->_table, $obj, '', true);
		}

		public function get_correct_answer($test_id, $student_id, $session){
			// echo 'session: '.$session;
			// $key_answers = $this->_conn->retrieve('multiple_question', array('test_id' => $test_id), array('key_answer', 'question_id'));
			// $student_answers = $this->_conn->retrieve('student_answer', array('test_id' => $test_id, 'student_id' => $student_id, 'session' => $session), 'answer');
			// var_dump($student_answers);
			$query = "SELECT sa.answer FROM student_answer sa, multiple_question mq WHERE mq.key_answer=sa.answer AND mq.test_id=$test_id AND sa.test_id=$test_id AND sa.student_id=$student_id AND sa.question_id=mq.question_id AND sa.session='$session'";
			return $this->_conn->nativeQuery($query);
		}

		public function pass($student_id, $test_id) {
			return $this->_conn->retrieve($this->_table, array('student_id' => $student_id, 'test_id' => $test_id, 'pass' => true), 'pass');
		}

		public function latest_test($student_id, $course_id) {
			$query = "	SELECT 	max(sequence) as latest
						FROM 	score s, test t
						WHERE 	s.test_id=t.test_id AND
								s.student_id=$student_id AND
								t.course_id=$course_id AND
								s.pass=true";
			return $this->_conn->nativeQuery($query);
		}
	}
?>