<?php
	Class Student_mdl extends CI_Model {
		private $_table = 'student';
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function login($obj) {
			return $this->_conn->retrieve($this->_table, $obj);
		}

		public function check_student_id($student_id) {
			$result = $this->_conn->retrieve($this->_table, array('student_id' => $student_id));
			return ($result != NULL) ? true : false;
		}

		public function register($obj) {
			return $this->_conn->save($obj, $this->_table);
		}

		public function get_student_scores() {
			$query = "	SELECT c.name, t.test_id, t.sequence, st.name as student_name, sc.student_id, max(sc.score) as score, count(sc.student_id) as try
						FROM student st, score sc, test t, course c
						WHERE st.student_id=sc.student_id
							AND t.test_id=sc.test_id
						    AND t.course_id = c.course_id 
						Group by sc.student_id, sc.test_id";
			return $this->_conn->nativeQuery($query);
		}
	}
?>