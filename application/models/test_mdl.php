<?php
	Class Test_mdl extends CI_Model {
		private $_table = 'test';
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add($obj) {
			return $this->_conn->save($obj, $this->_table, true);
		}

		public function get($obj='') {
			return $this->_conn->retrieve($this->_table, $obj, '', true);
		}

		public function detailed_test() {
			$result = $this->_conn->retrieve('course', '', '', true);
			for($i = 0; $i < count($result); $i++) {
				$result[$i]->tests = $this->_conn->retrieve($this->_table, array('course_id' => $result[$i]->course_id), '', true);
			}
			return $result;
		}

		public function update_test($obj) {
			return $this->_conn->edit($obj['update'], $this->_table, $obj['where']);
		}
	}
?>