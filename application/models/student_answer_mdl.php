<?php
	Class Student_answer_mdl extends CI_Model {
		private $_table = 'student_answer';
		private $_conn;

		public function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add($obj) {
			return $this->_conn->save($obj, $this->_table);
		}

		public function get($obj='') {
			$result =  $this->_conn->retrieve($this->_table, $obj, '', true);
		}
	}
?>