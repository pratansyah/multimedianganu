<?php
	function assets_url() {
		return 'http://localhost/multimedianganu/assets/';
	}

	if ( ! function_exists('encrypt')) {
		function encrypt($str) {
			$skey = "l4ndAk__Bun7Un9Z";
			if(!$str){return false;}
	        $text = $str;
	        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $skey, $text, MCRYPT_MODE_ECB, $iv);
	        return trim(safe_b64encode($crypttext));
		}
	}

	if ( ! function_exists('decrypt')) {
		function decrypt($str) {
			$skey = "l4ndAk__Bun7Un9Z";
			if(!$str){return false;}
	        $crypttext = safe_b64decode($str); 
	        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $skey, $crypttext, MCRYPT_MODE_ECB, $iv);
	        return trim($decrypttext);
		}
	}

	if ( ! function_exists('safe_b64encode')) {
		function safe_b64encode($str) {
	 
	        $data = base64_encode($str);
	        $data = str_replace(array('+','/','='),array('-','_',''),$data);
	        return $data;
	    }
	}
	 
	if ( ! function_exists('safe_b64decode')) {
		function safe_b64decode($str) {
	        $data = str_replace(array('-','_'),array('+','/'),$str);
	        $mod4 = strlen($data) % 4;
	        if ($mod4) {
	            $data .= substr('====', $mod4);
	        }
	        return base64_decode($data);
	    }	
	}

	if ( ! function_exists('excerpt')) {
		function excerpt($str, $limit = 65) {
			$excerpt = (strlen($str) > $limit) ? substr($str, 0, $limit).'... ' : $str;
			$last_found = -1;
			while(preg_match_all('~f\-video\-editor~i', $excerpt, $dummy) != $last_found) {
				// echo $last_found;
				$last_found = preg_match_all('~f\-video\-editor~i', $excerpt, $dummy);
				// echo $last_found;
				$excerpt = substr($str, 0, $limit+(500*preg_match_all('~f\-video\-editor~i', $excerpt, $dummy)));
				// $last_found = preg_match_all('~f\-video\-editor~i', $excerpt);
				// echo $last_found;die();
			}
			// $excerpt = (preg_match('~f\-video-editor"~i', $excerpt) != 0) ? substr($str, 0, $limit+(500*preg_match('~f\-video-editor~i', $excerpt))) : $excerpt;
			// $str = preg_replace('~\<video[\sa-z\="_0-9\-{&;\:}%]*>.*</video>~i', '', $str);
			return $excerpt;
		}
	}

	if ( ! function_exists('test_name')) {
		function test_name($sequence) {
			switch ($sequence) {
				case 1:
					$result = 'Pre test';
					break;
				
				case 2:
					$result = 'Latihan 1';
					break;
				
				case 3:
					$result = 'Latihan 2';
					break;
				
				case 4:
					$result = 'Post test';
					break;
				
				default:
					$result = 'unindexed';
					break;
			}
			return $result;
		}
	}

	if ( ! function_exists('convert_date')) {
		function convert_date($strDate) {
			$date_chunks = explode('-', array_shift(explode(' ', $strDate)));
			$date = (substr($date_chunks[2], 0, 1) == '0') ? substr($date_chunks[2], 1, 1) : $date_chunks[2];
			$year = $date_chunks[0];
			switch ($date_chunks[1]) {
				case '01':
					$month = 'Januari';
					break;

				case '02':
					$month = 'Februari';
					break;

				case '03':
					$month = 'Maret';
					break;

				case '04':
					$month = 'April';
					break;

				case '05':
					$month = 'Mei';
					break;

				case '06':
					$month = 'Juni';
					break;
				
				case '07':
					$month = 'Juli';
					break;
				
				case '08':
					$month = 'Agustus';
					break;
				
				case '09':
					$month = 'September';
					break;
				
				case '10':
					$month = 'Oktober';
					break;
				
				case '11':
					$month = 'November';
					break;
				
				case '12':
					$month = 'Desember';
					break;
				
				default:
					$month = 'Error';
					break;
			}

			return "$date $month $year";
		}
	}
?>