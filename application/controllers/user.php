<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student_mdl', 'student');
		$this->load->model('user_mdl', 'user');
		$this->load->model('test_mdl', 'test');
		$this->load->model('score_mdl', 'score');
		if(!$this->session->userdata('student')) redirect('login');
	}

	public function index() {
		$data['content']	= 'user/profile';
		$data['active']		= 'profile';
		$data['student']	= $this->session->userdata('student');
		$this->load->view('main', $data);
	}

	public function course() {
		$data['content']	= 'user/course';
		$data['active']		= 'course';
		$data['student']	= $this->session->userdata('student');
		$data['courses']	= $this->user->detailed_course();
		$this->load->view('main', $data);
	}

	public function test() {
		$data['content']	= 'user/test';
		$data['active']		= 'test';
		$data['student']	= $this->session->userdata('student');
		$data['courses']	= $this->test->detailed_test();
		$scores = $this->score->get(array('student_id' => $data['student']->student_id));
		if($scores) {
			foreach($scores as $score) {
				$data['scores'][$score->test_id][] = $score;
			}
		}
		else {
			$data['scores'] = array();
		}
		// var_dump($data['tests']);die();
		$this->load->view('main', $data);
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */