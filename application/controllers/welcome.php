<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->config('values');
	}
	public function index()	{
		// $this->load->view('welcome_message');
		$this->load->view('main', array('content' => 'home'));
	}

	public function encrypt($password) {
		// var_dump($this->config);die();
		echo encrypt($password)."</br>";
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */