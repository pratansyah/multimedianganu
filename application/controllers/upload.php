<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student_mdl', 'student');
		$this->load->library('session');
	}

	public function index() {
		$data['content']	= 'profile_owner';
		$data['student']	= $this->session->userdata('student');
		$this->load->view('main', $data);
	}

	public function froala() {
		if($_FILES['froala_image']) {
			$upload_path = realpath(dirname(__FILE__)).'/../../assets/images/content';
			$config['upload_path'] = $upload_path;
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '500';
			$config['file_name'] = time().'.jpg';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('froala_image')) {
				$error = array('error' => $this->upload->display_errors());

				var_dump($error);
			}
			else {
				$data = array('upload_data' => $this->upload->data());

				echo json_encode(array("link" => assets_url()."images/content/".$data['upload_data']['file_name']));
			}
		}
	}

	public function plupload() {
		if($_FILES['file']) {
			$upload_path = realpath(dirname(__FILE__)).'/../../assets/videos';
			$config['upload_path'] = $upload_path;
			$config['allowed_types'] = 'mkv|mp4|avi|flv';
			$config['max_size']	= '102400';
			$config['file_name'] = time().'.'.array_pop(explode('.', $_FILES['file']['name']));

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('file')) {
				$error = array('error' => $this->upload->display_errors());

				var_dump($error);
			}
			else {
				$data = array('upload_data' => $this->upload->data());

				echo json_encode(array("link" => assets_url()."videos/".$data['upload_data']['file_name']));
			}
			// var_dump($this->upload->data());
		}
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */