<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}
	public function index()	{
		if($this->input->post()) {
			if($this->input->post('username') == 'admin' && $this->input->post('password') == 'admin') {
				$this->session->set_userdata('admin', true);
				redirect('admin/course');
			}
		}
		$this->load->view('main', array('content' => 'login_admin'));
	}

}

/* End of file index.php */
/* Location: ./application/controllers/admin/index.php */