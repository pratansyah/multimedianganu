<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lesson extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('course_mdl', 'course');
		$this->load->model('lesson_mdl', 'lesson');
		if(!$this->session->userdata('admin')) redirect('admin');
	}
	public function index()	{
		$data['courses'] = $this->course->get();
		$data['content'] = 'admin/course/list';
		$this->load->view('main', $data);
	}
	public function add($course_id=-1) {
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('name', 'Nama materi', 'required');
			$this->form_validation->set_rules('course', 'Nama pelajaran', 'required');
			$this->form_validation->set_rules('content', 'Konten materi', 'required|xss_clean');

			if($this->form_validation->run() == TRUE) {
				$course = array();
				$lesson['name'] = $this->input->post('name');
				$lesson['content'] = $this->input->post('content');
				$lesson['course_id'] = $this->input->post('course');
				if ($this->lesson->add($lesson)) redirect('admin/course/detail/'.$lesson['course_id']);
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		$data['course'] = array_pop($this->course->get(array('course_id' => $course_id)));
		$data['course_list'] = $this->course->get();
		$data['content'] = 'admin/lesson/add';
		$this->load->view('main', $data);
	}

	public function edit($lesson_id=-1) {
		$data['lesson'] = array_pop($this->lesson->get(array('lesson_id' => $lesson_id)));
		// var_dump($data['lesson']);die();
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('name', 'Nama materi', 'required');
			$this->form_validation->set_rules('content', 'Konten materi', 'required!xss_clean');

			if($this->form_validation->run() == TRUE) {
				$course = array();
				$lesson['name'] = $this->input->post('name');
				$lesson['content'] = $this->input->post('content');
				$lesson['lesson_id'] = $data['lesson']->lesson_id;
				// var_dump($lesson);die();
				if ($this->lesson->edit($lesson, $lesson_id)) redirect('admin/course/detail/'.$data['lesson']->course_id);
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		$data['course'] = $this->course->get(array('course_id' => $data['lesson']->course_id));
		$data['content'] = 'admin/lesson/edit';
		$this->load->view('main', $data);
	}
	public function detail($course_id) {
		$data['content'] 	= 'admin/course/detail';
		$data['course']		= $this->course->get(array('course_id' => $course_id)); 
		$this->load->view('main', $data);
	}

}

/* End of file course.php */
/* Location: ./application/controllers/admin/course.php */