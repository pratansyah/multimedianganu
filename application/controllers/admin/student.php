<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student_mdl', 'student');
	}
	public function index()	{
		$data['content']	= 'admin/student/list';
		$students	= $this->student->get_student_scores();
		if($students):
			foreach ($students as $student) {
				$data['students'][$student->student_id][$student->name][$student->sequence] = $student;
				$data['students_name'][$student->student_id] = $data['students'][$student->student_id][$student->name][$student->sequence]->student_name;
			}
		endif;
		// echo "<pre>";
		// var_dump($data['students']);die();
		$this->load->view('main', $data);
	}


}

/* End of file student.php */
/* Location: ./application/controllers/admin/student.php */