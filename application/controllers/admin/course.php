<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class course extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('course_mdl', 'course');
		$this->load->model('lesson_mdl', 'lesson');
		$this->load->model('test_mdl', 'test');
		if(!$this->session->userdata('admin')) redirect('admin');
	}
	public function index()	{
		$data['courses'] = $this->course->get();
		$data['content'] = 'admin/course/list';
		$this->load->view('main', $data);
	}
	public function add() {
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('name', 'Nama pelajaran', 'required');
			$this->form_validation->set_rules('description', 'Deskripsi singkat', 'required');

			if($this->form_validation->run() == TRUE) {
				$course = array();
				$course['name'] = $this->input->post('name');
				$course['description'] = $this->input->post('description');
				if ($this->course->add($course)) redirect('admin/course');
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		$this->load->view('main', array('content' => 'admin/course/add'));
	}

	public function detail($course_id) {
		$data['content'] 	= 'admin/course/detail';
		$data['course']		= array_pop($this->course->get(array('course_id' => $course_id)));
		$data['lessons']	= $this->lesson->get(array('course_id' => $course_id));
		$tests				= $this->test->get(array('course_id' => $course_id));
		if($tests) {
			foreach ($tests as $test) {
				$data['tests'][$test->sequence] = $test;
			}
		}
		else {
			$data['tests'] = array();
		}
		$data['tests_key'][1] = 'Pre test';
		$data['tests_key'][2] = 'Latihan 1';
		$data['tests_key'][3] = 'Latihan 2';
		$data['tests_key'][4] = 'Post test';
		// $data['tests']		
		$this->load->view('main', $data);
	}

}

/* End of file course.php */
/* Location: ./application/controllers/admin/course.php */