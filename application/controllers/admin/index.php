<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}
	public function index()	{
		if($this->input->post()) {
			if($this->input->post('username') == 'admin' && $this->input->post('password') == 'admin') {
				$this->session->set_userdata('admin', true);
				redirect('admin/course');
			}
		}
		$this->load->view('main', array('content' => 'login_admin'));
	}
	public function logout() {
		$this->session->unset_userdata('admin');
		redirect();
	}

}

/* End of file index.php */
/* Location: ./application/controllers/admin/index.php */