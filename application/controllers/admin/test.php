<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('test_mdl', 'test');
		$this->load->model('question_mdl', 'question');
		$this->load->model('answer_mdl', 'answer');
		if(!$this->session->userdata('admin')) redirect('admin');
	}
	public function index()	{

	}
	public function add($course_id, $sequence, $type) {
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('duration', 'Durasi', 'required');
			$this->form_validation->set_rules('jumlah_soal', 'Jumlah Soal', 'required');
			$this->form_validation->set_rules('passing_grade', 'Passing Grade', 'required');

			if($this->form_validation->run() == TRUE) {
				$this->session->unset_userdata('test');
				$course = array();
				$test['time_allocation'] = $this->input->post('duration');
				$test['questions_number'] = $this->input->post('jumlah_soal');
				$test['passing_grade'] = $this->input->post('passing_grade');
				$test['course_id'] = $course_id;
				$test['sequence'] = $sequence;
				$test['type'] = $type;
				$test_id = $this->test->add($test);
				if ($test_id != false) {
					$test['test_id'] = $test_id;
					$this->session->set_userdata('test', $test);
					redirect('admin/test/add_question/');
				}
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		$data['content']	= 'admin/test/add';
		// $predefined = new stdClass();
		// $predefined->sequence = $sequence;
		// $predefined->course_id = $course_id;
		// $predefined->type = $type;
		// $data['predefined']	= $predefined;
		$this->load->view('main', $data);
	}

	public function add_exist($course_id, $sequence, $type) {
		$current_test	= array_pop($this->test->get(array('course_id' => $course_id, 'sequence' => $sequence)));
		// var_dump($current_test);die();
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('duration', 'Durasi', 'required');

			if($this->form_validation->run() == TRUE) {
				$this->session->unset_userdata('test');
				$course = array();
				$test['update']['time_allocation'] = $this->input->post('duration')+$current_test->time_allocation;
				$test['update']['questions_number'] = $this->input->post('jumlah_soal')+$current_test->questions_number;
				$test['where']['course_id'] = $course_id;
				$test['where']['sequence'] = $sequence;
				$test['where']['test_id'] = $current_test->test_id;
				$test['where']['type'] = $type;
				$update = $this->test->update_test($test);
				if ($update != false) {
					$test['update']['test_id'] = $current_test->test_id;
					$test['update']['course_id'] = $current_test->course_id;
					$this->session->set_userdata('test', $test['update']);
					redirect('admin/test/add_question/'.($current_test->questions_number+1));
				}
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		$data['content']	= 'admin/test/add_exist';
		$this->load->view('main', $data);
	}

	public function edit($test_id) {
		$current_test	= array_pop($this->test->get(array('test_id' => $test_id)));
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('duration', 'Durasi', 'required');
			$this->form_validation->set_rules('passing_grade', 'Passing grade', 'required');

			if($this->form_validation->run() == TRUE) {
				$this->session->unset_userdata('test');
				$course = array();
				$test['update']['time_allocation'] = $this->input->post('duration');
				$test['update']['passing_grade'] = $this->input->post('passing_grade');
				$test['where']['course_id'] = $current_test->course_id;
				$test['where']['test_id'] = $current_test->test_id;
				$update = $this->test->update_test($test);
				if ($update != false) {
					$test['update']['test_id'] = $current_test->test_id;
					$test['update']['course_id'] = $current_test->course_id;
					$test['update']['questions_number'] = $current_test->questions_number;
					$this->session->set_userdata('test', $test['update']);
					redirect('admin/test/edit_question/');
				}
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}
		$data['content']	= 'admin/test/edit';
		$data['test']		= $current_test;
		$this->load->view('main', $data);
	}

	public function add_question($sequence = 1) {
		$data['content'] = 'admin/test/add_question';
		$data['sequence'] = $sequence;
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('key', 'Kunci jawaban', 'required');
			$this->form_validation->set_rules('question', 'Pertanyaan', 'required');

			if($this->form_validation->run() == TRUE) {
				$test = $this->session->userdata('test');
				$course = array();
				$question['question'] = $this->input->post('question');
				$question['key_answer'] = $this->input->post('key');
				$question['note'] = $this->input->post('note');
				$question['sequence'] = $sequence;
				$question['test_id'] = $test['test_id'];
				$question_id = $this->question->add($question);
				if ($question_id != false) {
					if($this->input->post('a')) $this->answer->add(array('question_id' => $question_id, 'header' => 'a', 'content' => $this->input->post('a')));
					if($this->input->post('b')) $this->answer->add(array('question_id' => $question_id, 'header' => 'b', 'content' => $this->input->post('b')));
					if($this->input->post('c')) $this->answer->add(array('question_id' => $question_id, 'header' => 'c', 'content' => $this->input->post('c')));
					if($this->input->post('d')) $this->answer->add(array('question_id' => $question_id, 'header' => 'd', 'content' => $this->input->post('d')));
					if($this->input->post('e')) $this->answer->add(array('question_id' => $question_id, 'header' => 'e', 'content' => $this->input->post('e')));
					($test['questions_number'] > $sequence) ? redirect('admin/test/add_question/'.($sequence+1)) : redirect('admin/course/detail/'.$test['course_id']);
				}
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}

		$this->load->view('main', $data);
	}

	public function edit_question($sequence = 1) {
		$test = $this->session->userdata('test');
		$data['content'] 	= 'admin/test/edit_question';
		$data['sequence'] 	= $sequence;
		$data['question'] 	= $this->question->get(array('test_id' => $test['test_id'], 'sequence' => $sequence));
		$answers		= $this->answer->get(array('question_id' => $data['question']->question_id));
		foreach($answers as $answer) {
			$data['answer'][$answer->header] = $answer->content;
		}

		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', 'Silahkan isi %s');
			$this->form_validation->set_rules('key', 'Kunci jawaban', 'required');
			$this->form_validation->set_rules('question', 'Pertanyaan', 'required');

			if($this->form_validation->run() == TRUE) {
				$course = array();
				$question['update']['question'] = $this->input->post('question');
				$question['update']['key_answer'] = $this->input->post('key');
				$question['update']['note'] = $this->input->post('note');
				$question['where']['sequence'] = $sequence;
				$question['where']['test_id'] = $test['test_id'];
				$question_id = $this->question->edit($question);
				if ($question_id != false) {
					$headers = array('a', 'b', 'c', 'd', 'e');
					foreach($headers as $header) {
						if($this->input->post($header)) $this->answer->edit(array('update' => array('content' => $this->input->post($header)), 'where' => array('question_id' => $data['question']->question_id, 'header' => $header)));
					}					
					($test['questions_number'] > $sequence) ? redirect('admin/test/edit_question/'.($sequence+1)) : redirect('admin/course/detail/'.$test['course_id']);
				}
				else $this->session->set_flashdata('error', 'Terjadi kesalahan, silahkan ulangi');
			}
		}

		$this->load->view('main', $data);
	}

	public function set_session_question() {
		$test['time_allocation'] = 20;
		$test['questions_number'] = 10;
		$test['passing_grade'] = 75;
		$test['course_id'] = 1;
		$test['sequence'] = 1;
		$test['type'] = 'multiple';
		$test['test_id'] = 1;
		$this->session->set_userdata('test', $test);
	}

}

/* End of file Test.php */
/* Location: ./application/controllers/admin/Test.php */