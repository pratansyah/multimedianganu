<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student_mdl', 'student');
		$this->load->library('session');
	}

	public function index()	{
		if($this->input->post()) {
			$student = array();
			$student['student_id'] = $this->input->post('nis');
			$student['password'] = encrypt($this->input->post('password'));
			$result = $this->student->login($student);
			if($result == NULL){
				$this->session->set_flashdata('error', 'Nomor Induk dan atau password salah, silahkan coba lagi');
			}
			else {
				$this->session->set_userdata('student', $result);
				redirect('user');
			}
		}
		$this->load->view('main', array('content' => 'login'));
	}

	public function register() {
		if($this->input->post()) {
			$student = array();
			$student['student_id'] = ($this->input->post('nis')) ? $this->input->post('nis') : NULL;
			$student['name'] = ($this->input->post('name')) ? $this->input->post('name') : NULL;
			$student['password'] = ($this->input->post('password')) ? encrypt($this->input->post('password')) : NULL;
			foreach ($student as $value) {
				if($value == NULL) {
					$this->session->set_flashdata('error', 'Silahkan lengkapi data');
					redirect('register');
				}
			}
			if(!$this->student->check_student_id($student['student_id'])) {
				if($this->student->register($student)) {
					$student = $this->student->login($student);
					$this->session->set_userdata('student', $student);
					redirect('user');
				}
			}
			else {
				$this->session->set_flashdata('error', 'Nomor Induk sudah dipakai');
				redirect('register');
			}
		}
		$this->load->view('main', array('content' => 'register'));
	}

	public function logout() {
		$this->session->unset_userdata('student');
		redirect('');
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */