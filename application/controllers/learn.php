<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Learn extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student_mdl', 'student');
		$this->load->model('question_mdl', 'question');
		$this->load->model('answer_mdl', 'answer');
		$this->load->model('lesson_mdl', 'lesson');
		$this->load->model('course_mdl', 'course');
		$this->load->model('test_mdl', 'test');
		$this->load->model('score_mdl', 'score');
		$this->load->model('student_answer_mdl', 'student_answer');
		$this->load->model('student_lesson_mdl', 'student_lesson');
	}

	public function index() {
		redirect('user');
	}

	private function check_progress($course_id, $type, $sequence) {
		if($this->session->userdata('student')) {
			$student = $this->session->userdata('student');
			$lesson_learned 		= array_pop($this->student_lesson->latest_lesson($student->student_id,$course_id));
			$lesson_learned->latest = ($lesson_learned->latest) ? $lesson_learned->latest : 0;
			$test_passed 			= array_pop($this->score->latest_test($student->student_id,$course_id));
			$test_passed->latest 	= ($test_passed->latest) ? $test_passed->latest : 0;
			// var_dump($lesson_learned->latest.' '.$sequence.' '.$type);die();

			// if($test_passed->latest == 0 && $type == 'lesson') return false; // Blank student trying to access lesson
			if($lesson_learned->latest == 0 && $type == 'test' && $sequence == 1) return true; // Blank student trying to access pre-test
			if($lesson_learned->latest == 2 && $type == 'test' && $sequence == 4) return true; // Student trying to access post-test
			if($type == 'lesson') {
				return ($test_passed->latest >= $sequence) ? true : false;
			}
			elseif($type == 'test') {
				return ($lesson_learned->latest+1 >= $sequence) ? true : false;
			}
		}
		elseif($this->session->userdata('admin')) return true;
	}

	public function lesson($lesson_id=0) {
		$data['lesson'] 	= array_pop($this->lesson->get(array('lesson_id' => $lesson_id)));
		if(!$this->check_progress($data['lesson']->course_id, 'lesson', $data['lesson']->sequence)) {
			$data['content']	= 'learn/misprogress';
			$data['type']		= 'materi';
			$data['url']		= base_url().'user/course';
			$this->load->view('main', $data);
		}
		else {
			if(!$data['lesson']) redirect('user');
			$data['course']		= array_pop($this->course->get(array('course_id' => $data['lesson']->course_id)));
			$data['subheader']	= $data['lesson']->name;

			if($this->session->userdata('student')) {
				$student_lesson = array();
				$student_lesson['student_id'] 	= $this->session->userdata('student')->student_id;
				$student_lesson['lesson_id']	= $lesson_id;
				$this->student_lesson->add($student_lesson);
			}

			$lessons = $this->lesson->get(array('course_id' => $data['lesson']->course_id));
			$sidebar = array();
			foreach($lessons as $lesson) {
				$item = new stdClass();
				$item->header = $lesson->name;
				$item->content = $lesson->content;
				$item->url = base_url().'learn/lesson/'.$lesson->lesson_id;
				$item->checked = ($lesson->lesson_id == $data['lesson']->lesson_id) ? true : false;
				$sidebar[$lesson->lesson_id] = $item;
			}
			ksort($sidebar);

			$data['sidebar']	= $sidebar;
			$data['content']	= 'lesson';
			$this->load->view('learn/main', $data);
		}
	}

	public function test($test_id, $sequence=0) {
		$status = $this->score->pass($this->session->userdata('student')->student_id, $test_id);
		// var_dump($status);die();
		if($status != null && $status->pass == true) {
			redirect('learn/review/'.$test_id);
		}
		else {
			$data['test_info'] 	= array_pop($this->test->get(array('test_id' => $test_id)));
			if(!$this->check_progress($data['test_info']->course_id, 'test', $data['test_info']->sequence)) {
				$data['content']	= 'learn/misprogress';
				$data['type']		= 'tes';
				$data['url']		= base_url().'user/test';
				$this->load->view('main', $data);
			}
			else {
				$data['subheader']	= test_name($data['test_info']->sequence);

				if($this->input->post()) {
					$time_left= $this->input->post('time_left');
					$answer['answer'] = $this->input->post('answer');
					$answer['question_id'] = $this->input->post('question_id');
					$answer['student_id'] = $this->session->userdata('student')->student_id;
					$answer['session'] = $this->session->userdata('session');
					$answer['test_id'] = $test_id;
					$this->student_answer->add($answer);
				}

				if($sequence > $data['test_info']->questions_number) {
					redirect('learn/score_test/'.$test_id.'/1');
				}

				$data['test_info']->sequence = $sequence;
				$data['course']		= array_pop($this->course->get(array('course_id' => $data['test_info']->course_id))); 
				$tests = $this->test->get();
				if($tests) {
					foreach($tests as $test) {
						$item = new stdClass();
						$item->header = test_name($test->sequence);
						$item->content = $test->type;
						$item->url = '#';
						$item->checked = false;
						$sidebar[$test->sequence] = $item;
					}
					ksort($sidebar);
					$data['sidebar']	= $sidebar;
				}
				else $data['sidebar'] = array();
				$data['test_info']->current = $sequence;

				if($sequence > 0) {
					$data['question'] 	= $this->question->get(array('test_id' => $test_id, 'sequence' => $sequence));
					$data['answers']	= $this->answer->get(array('question_id' => $data['question']->question_id));
				}
				if($sequence == 0) {
					$data['content'] = 'test_intro';
					$session = encrypt(time());
					$this->session->set_userdata('session', $session);
				}
				elseif($sequence == 1) {
					$data['time_left'] = ($data['test_info']->time_allocation * 60);
					$data['content'] = 'test';
				}
				else $data['content'] = 'test';
				$this->load->view('learn/main', $data);
			}
		}
	}

	public function review($test_id, $sequence=1) {
		$data['test_info'] 	= array_pop($this->test->get(array('test_id' => $test_id)));
		if($sequence > $data['test_info']->questions_number) redirect('user/test');
		$data['subheader']	= test_name($data['test_info']->sequence);
		$data['test_info']->sequence = $sequence;
		$data['course']		= array_pop($this->course->get(array('course_id' => $data['test_info']->course_id))); 
		$tests = $this->test->get();
		if($tests) {
			foreach($tests as $test) {
				$item = new stdClass();
				$item->header = test_name($test->sequence);
				$item->content = $test->type;
				$item->url = '#';
				$item->checked = false;
				$sidebar[$test->sequence] = $item;
			}
			ksort($sidebar);
			$data['sidebar']	= $sidebar;
		}
		else $data['sidebar'] = array();
		$data['test_info']->current = $sequence;
		$data['test_info']->test_id = $test_id;
		// var_dump($data);die();
		$data['question'] 	= $this->question->get(array('test_id' => $test_id, 'sequence' => $sequence));
		// var_dump($data['question']);die();
		$data['answers']	= $this->answer->get(array('question_id' => $data['question']->question_id));
		$data['content'] 	= 'review';
		$this->load->view('learn/main', $data);

	}

	public function score_test($test_id) {
		$right_answer = $this->score->get_correct_answer($test_id, $this->session->userdata('student')->student_id, $this->session->userdata('session'));
		$test_info = array_pop($this->test->get(array('test_id' => $test_id)));
		$score = array();
		$score['student_id'] = $this->session->userdata('student')->student_id;
		$score['test_id'] = $test_id;
		$score['session'] = $this->session->userdata('session');
		$score['score'] = count($right_answer)/$test_info->questions_number * 100;
		$score['pass'] = ($score['score'] > $test_info->passing_grade) ? true : false;
		if($this->score->add($score)) redirect('user/test');
	}
}

/* End of file learn.php */
/* Location: ./application/controllers/learn.php */