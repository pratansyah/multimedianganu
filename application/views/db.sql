create table student(
	student_id varchar(10) primary key,
	name varchar(100),
	image varchar(100),
	password varchar(255)
);

create table course(
	course_id int(5) primary key auto_increment,
	name varchar(100),
	description varchar(255)
);

create table lesson(
	lesson_id int(5) primary key auto_increment,
	course_id int(5),
	sequence int(3),
	name varchar(100),
	content text
);

create table test(
	test_id int(5) primary key auto_increment,
	course_id int(5),
	name varchar(10),
	type varchar(50),
	time_allocation int(10),
	passing_grade int(4)
);

create table multiple_question(
	question_id int(10) primary key auto_increment,
	test_id int(5),
	sequence int(4),
	question text,
	key_answer varchar(5)
);

create table question_group (
	question_group_id int(5) primary key auto_increment,
	test_id int(5),
	content text
);

create table fill_question(
	question_id int(10) primary key auto_increment,
	question_group_id int(10),
	sequence int(4),
	key_answer varchar(5)
);

create table answer(
	answer_id int(10) primary key auto_increment,
	question_id int(10),
	header varchar(5),
	content text
);

create table student_answer (
	student_answer_id int(5) primary key auto_increment,
	student_id int(5),
	test_id int(5),
	answer varchar(5)
);

create table score(
	score_id int(5) primary key auto_increment,
	student_id int(5),
	test_id int(5),
	score int(4)
);