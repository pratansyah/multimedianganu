<!-- COURSE CONCERN -->
<section id="course-concern" class="course-concern">
    <div class="container">
        
        <div class="table-asignment">


            <!-- Tab panes -->
            <div class="tab-content">
                <!-- MY SUBMISSIONS -->
                <div class="tab-pane fade in active" id="mysubmissions">
                    <div class="table-wrap">
                        <div class="table-head">
                            <div class="submissions"></div>
                            <div class="total-subm">Nilai/Jumlah coba</div>
                            <div class="replied">Passing grade</div>
                            <div class="latest-reply">Dibuat</div>
                            <div class="tb-icon"></div>
                        </div>

                        <!-- TABLE BODY -->
                        <div class="table-body">
                            <!-- TABLE ITEM -->
                            <?php if($courses): foreach($courses as $course): ?>
                            <div class="table-item">
                                <div class="thead">
                                    <div class="submissions"><?php echo $course->name ?></a></div>
                                    <div class="total-subm"></div>
                                    <div class="replied"></div>
                                    <div class="latest-reply"></div>
                                    <div class="toggle tb-icon">
                                        <a href="#"><i class="fa fa-angle-down"></i></a>
                                    </div>
                                </div>
                                <?php if($course->tests): foreach($course->tests as $test): ?>
                                <div class="tbody">
                                    <!-- ITEM -->
                                    <div class="item">
                                        <!-- <div class="submissions"><a href="<?php echo base_url().'learn/test/'.$test->test_id; ?>"><?php echo test_name($test->sequence); ?></a></div> -->
                                        <div class="submissions"><a href="#"><?php echo test_name($test->sequence); ?></a></div>
                                        <div class="total-subm">
                                            <?php
                                                if (isset($scores[$test->test_id])) {
                                                    $biggest_score = 0;
                                                    foreach($scores[$test->test_id] as $score) {
                                                        if($biggest_score <= $score->score) $biggest_score = $score->score;
                                                    }
                                                    echo $biggest_score.'/';
                                                    echo count($scores[$test->test_id]);
                                                }
                                                else echo '0/0';

                                            ?>
                                        </div>
                                        <div class="replied"><?php echo $test->passing_grade ?></div>
                                        <div class="latest-reply"><?php echo convert_date($test->created); ?></div>
                                        <div class="link tb-icon">
                                            <a href="<?php echo base_url().'learn/test/'.$test->test_id; ?>"><i class="fa fa-play-circle-o"></i></a>
                                        </div>
                                    </div>
                                    <!-- END / ITEM -->

                                </div>
                                <?php endforeach; endif;?>
                            </div>
                            <?php endforeach; endif; ?>
                            <!-- END / TABLE ITEM -->
                            
                        </div>
                        <!-- END / TABLE BODY -->
                    </div>

                </div>
                <!-- END / MY SUBMISSIONS -->

            </div>

        </div>
    </div>
</section>
<!-- END / COURSE CONCERN -->

<script type="text/javascript">

$.each($('.table-wrap'), function() {
    $(this)
        .find('.table-item')
        .children('.thead:not(.active)')
        .next('.tbody').hide();
    $(this)
        .find('.table-item')
        .delegate('.thead', 'click', function(evt) {
            evt.preventDefault();
            if ($(this).hasClass('active')==false) {
                $('.table-item')
                    .find('.thead')
                    .removeClass('active')
                    .siblings('.tbody')
                        .slideUp(200);
            }
            $(this)
                .toggleClass('active')
                .siblings('.tbody')
                    .slideToggle(200);
    });
});

</script>