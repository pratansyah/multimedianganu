    <!-- PROFILE FEATURE -->
    <section class="profile-feature">
        <div class="awe-parallax bg-profile-feature"></div>
        <div class="awe-overlay overlay-color-3"></div>
        <div class="container">
            <div class="info-author">
                <div class="image">
                    <img src="<?php echo assets_url(); ?>images/profile/<?php echo $student->image; ?>" alt="">
                </div>
                <div class="name-author">
                    <h2 class="big"><?php echo $student->name; ?></h2>
                </div>
            </div>
        </div>
    </section>
    <!-- END / PROFILE FEATURE -->


    <!-- CONTENT BAR -->
    <section class="content-bar">
        <div class="container">
            <ul>
                <li <?php if($active == 'course') echo "class='current'"; ?>>
                    <a href="<?php echo base_url().'user/course' ?>">
                        <i class="icon md-book-1"></i>
                        Pelajaran
                    </a>
                </li>
                <li <?php if($active == 'test') echo "class='current'"; ?>>
                    <a href="<?php echo base_url().'user/test'; ?>">
                        <i class="icon md-shopping"></i>
                        Tes
                    </a>
                </li>
                <li <?php if($active == 'profile') echo "class='current'"; ?>>
                    <a href="<?php echo base_url().'user'; ?>">
                        <i class="icon md-user-minus"></i>
                        Profil
                    </a>
                </li>
            </ul>
        </div>
    </section>
   <!-- END / CONTENT BAR -->