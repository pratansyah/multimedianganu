<!-- COURSE CONCERN -->
<section id="course-concern" class="course-concern">
    <div class="container">
        
        <div class="table-asignment">


            <!-- Tab panes -->
            <div class="tab-content">
                <!-- MY SUBMISSIONS -->
                <div class="tab-pane fade in active" id="mysubmissions">
                    <div class="table-wrap">
                        <!-- END / TABLE HEAD -->

                        <!-- TABLE BODY -->
                        <div class="table-body">
                            <!-- TABLE ITEM -->
                            <?php if($courses): foreach($courses as $course): ?>
                            <div class="table-item">
                                <div class="thead">
                                    <div class="submissions"><?php echo $course->name ?></a></div>
                                    <div class="total-subm"></div>
                                    <div class="replied"></div>
                                    <div class="latest-reply"></div>
                                    <div class="toggle tb-icon">
                                        <a href="#"><i class="fa fa-angle-down"></i></a>
                                    </div>
                                </div>
                                <?php if($course->lessons): foreach($course->lessons as $lesson): ?>
                                <div class="tbody">
                                    <!-- ITEM -->
                                    <div class="item">
                                        <div class="submissions"><a href="<?php echo base_url().'learn/lesson/'.$lesson->lesson_id; ?>"><?php echo $lesson->name ?></a></div>
                                        <div class="total-subm"></div>
                                        <div class="replied"></div>
                                        <div class="latest-reply"><?php echo $lesson->created; ?></div>
                                        <div class="link tb-icon">
                                            <a href="<?php echo base_url().'learn/lesson/'.$lesson->lesson_id; ?>"><i class="fa fa-play-circle-o"></i></a>
                                        </div>
                                    </div>
                                    <!-- END / ITEM -->

                                </div>
                                <?php endforeach; endif;?>
                            </div>
                            <?php endforeach; endif; ?>
                            <!-- END / TABLE ITEM -->
                            
                        </div>
                        <!-- END / TABLE BODY -->
                    </div>

                </div>
                <!-- END / MY SUBMISSIONS -->

            </div>

        </div>
    </div>
</section>
<!-- END / COURSE CONCERN -->

<script type="text/javascript">

$.each($('.table-wrap'), function() {
    $(this)
        .find('.table-item')
        .children('.thead:not(.active)')
        .next('.tbody').hide();
    $(this)
        .find('.table-item')
        .delegate('.thead', 'click', function(evt) {
            evt.preventDefault();
            if ($(this).hasClass('active')==false) {
                $('.table-item')
                    .find('.thead')
                    .removeClass('active')
                    .siblings('.tbody')
                        .slideUp(200);
            }
            $(this)
                .toggleClass('active')
                .siblings('.tbody')
                    .slideToggle(200);
    });
});

</script>