    <!-- PROFILE -->
    
    <section class="profile">
        <div class="container">
            <h3 class="md black">Profile</h3>
            <div class="row">
                <div class="col-md-10">
                    <div class="avatar-acount">
                        <div class="changes-avatar">
                            <div class="img-acount">
                                <img src="<?php echo assets_url(); ?>images/profile/<?php echo $student->image; ?>" alt="">
                            </div>
                            <div class="choses-file up-file">
                                <input type="file">
                                <input type="hidden">
                                <a href="account-profile-owner-view.html" class="mc-btn btn-style-6">Ganti gambar</a>
                            </div>
                        </div>   
                        <div class="info-acount">
                            <h3 class="md black"><?php echo $student->name; ?></h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                            <!-- <div class="security">
                                <div class="tittle-security">
                                    <h5>Email</h5>
                                    <input type="text">
                                    <h5>Password</h5>
                                    <input type="password" placeholder="Current password">
                                    <input type="password" placeholder="New password">
                                    <input type="password" placeholder="Confirm password">
                                </div>
                            </div> -->
                        </div>
                        <!-- <div class="input-save">   
                            <input type="submit" value="Save changes" class="mc-btn btn-style-1">
                        </div> -->
                    </div>    
                </div>
            </div>
        </div>
    </section>


    <!-- END / PROFILE -->