<!-- FEATURED REQUEST TEACHER -->
<section id="featured-request-teacher" class="featured-request-teacher">
    <div class="awe-parallax bg-featured-request-teacher"></div>
    <div class="awe-overlay overlay-color-1"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="request-form">
                    <h2>Login Admin</h2>
                    <form method="POST">
                        <div class="form-yourname">
                            <input type="text" placeholder="Username" name="username">
                        </div>
                        <div class="form-yourmail">
                            <input type="password" placeholder="Password" name="password">
                        </div>
                        <div class="form-submit-1">
                            <input type="submit" value="Send request" class="mc-btn btn-style-1">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END / FEATURED REQUEST TEACHER -->