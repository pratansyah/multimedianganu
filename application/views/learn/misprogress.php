
<section id="quizz-intro-section" class="quizz-intro-section learn-section">
    <div class="container">
        <div class="question-content-wrap">
            <div class="row">
                <div class="col-md-10">
                    <div class="question-content">
                        <h4 class="md">Intro</h4>
                        <p>Anda belum memenuhi persyaratan untuk mengambil <?php echo $type; ?> ini</p>
                        <div class="form-action">
                            <a href="<?php echo $url; ?>" class="mc-btn btn-style-1">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>