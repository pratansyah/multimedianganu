
<section id="quizz-intro-section" class="quizz-intro-section learn-section">
    <div class="container">


        <div class="question-content-wrap">
            <div class="row">
                <div class="col-md-8">
                    <div class="question-content">
                        <h4 class="md">Soal <?php echo $test_info->current; ?></h4>
                        <p><?php echo $question->question ?></p>
                        <div class="answer">
                            <h4 class="sm">Jawaban</h4>
                            <ul class="answer-list">
                                <?php foreach($answers as $answer): ?>
                                <li>
                                    <input type="radio" name="answer" value="<?php echo $answer->header; ?>" id="answer-<?php echo $answer->header; ?>" disabled <?php if($answer->header == $question->key_answer) echo 'checked' ?>>
                                    <label for="answer-<?php echo $answer->header; ?>">
                                        <i class="icon icon_radio"></i>
                                        <?php echo $answer->content; ?>
                                    </label>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <h4 class="md">Penjelasan</h4>
                        <p><?php echo $question->note ?></p>
                        <a class="mc-btn btn-style-1" href="<?php echo base_url().'learn/review/'.$test_info->test_id.'/'.(intval($test_info->sequence)+1); ?>">Selanjutnya</a>
                    </div>

                    
                </div>


                <div class="col-md-4">
                    <aside class="question-sidebar">
                        <div class="score-sb">
                            <ul>
                                <?php for($i = 1; $i <= $test_info->questions_number; $i++): ?>
                                    <li class="<?php echo ($test_info->current > $i) ? 'val' : (($test_info->current == $i) ? 'active' : ''); ?>"><i class="icon"></i>Soal <?php echo $i ?></li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>