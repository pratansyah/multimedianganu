
</div>
<!-- END / PAGE WRAP -->

<!-- Load jQuery -->
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery.owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery.appear.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/scripts.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/froala/js/froala_editor.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/froala/js/plugins/video.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/froala/js/plugins/tables.min.js"></script>

<script>
    $(function() {
        $('.froala-box').editable({
            inlineMode: false,
            buttons: ['bold', 'italic', 'sep', 'formatBlock', 'indent', 'outdent',
                'insertOrderedList', 'insertUnorderedList', 'sep',
                'createLink', 'insertHorizontalRule', 'table', 'insertImage', 'insertVideo', 'subscript', 'superscript', 'sep', 'undo', 'redo'],

            // Set the image upload parameter.
            imageUploadParam: 'froala_image',

            // Set the image upload URL.
            imageUploadURL: "<?php echo base_url().'upload/froala'; ?>",

            // Additional upload params.
            imageUploadParams: {id: 'my_editor'}
        })
    });
</script>
</body>
</html>