<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <!-- Google font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/library/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/library/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/library/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/md-font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/style.css">

    <!-- Include Editor style. -->
    <link href="<?php echo assets_url(); ?>js/library/froala/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo assets_url(); ?>js/library/froala/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery-1.11.0.min.js"></script>
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <title>Multimedia Nganu</title>
</head>
<body id="page-top" class="home">

<!-- PAGE WRAP -->
<div id="page-wrap">

    <div class="top-nav">

        <h4 class="sm black bold"><?php echo $course->name.': '.$subheader; ?></h4>

        <ul class="top-nav-list">
            <li class="prev-course"><a href="learning.html#"><i class="icon md-angle-left"></i><span class="tooltip">Prev</span></a></li>
            <li class="next-course"><a href="learning.html#"><i class="icon md-angle-right"></i><span class="tooltip">Next</span></a></li>
            <li class="outline-learn">
                <a href="learning.html#"><i class="icon md-list"></i></a>
                <div class="list-item-body outline-learn-body">
                    <div class="section-learn-outline">
                        <h5 class="section-title"><?php echo $course->name ?></h5>
                        <ul class="section-list">
                            <?php foreach($sidebar as $item): ?>
                            <li class="<?php if($item->checked) echo 'o-view'; ?>">
                                <div class="list-body">
                                    <a href="<?php echo $item->url; ?>">
                                        <h6><?php echo $item->header; ?></h6>
                                        <!-- <p>12 questions</p> -->
                                    </a>
                                </div>
                                <div class="div-x"><i class="icon md-check-2"></i></div>
                            </li>
                            <?php endforeach; ?>

                        </ul>
                    </div>

                </div>
            </li>

            <li class="backpage">
                <a href="<?php echo base_url().'user'; ?>"><i class="icon md-close-1"></i></a>
            </li>
        </ul>
        
    </div>