<section id="learning-section" class="learning-section learn-section">
    <div class="container">
        <div class="question-content-wrap">
            <div class="row">
                <div class="col-md-12">
                    <div class="question-content">
                        <h1><?php echo $lesson->name ?></h1>
                        <?php echo $lesson->content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>