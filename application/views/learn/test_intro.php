
<section id="quizz-intro-section" class="quizz-intro-section learn-section">
    <div class="container">


        <div class="question-content-wrap">
            <div class="row">
                <div class="col-md-8">
                    <div class="question-content">
                        <h4 class="md">Intro</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                        <div class="form-action">
                            <a href="<?php echo base_url().'learn/test/'.$test_info->test_id.'/1' ?>" class="mc-btn btn-style-1">Mulai tes</a>
                            <span class="total-time">Durasi : <?php echo $test_info->time_allocation; ?> menit</span>
                        </div>
                    </div>

                    
                </div>


                <div class="col-md-4">
                    <aside class="question-sidebar">
                        <div class="score-sb">
                            <ul>
                                <?php for($i = 1; $i <= $test_info->questions_number; $i++): ?>
                                    <li class="<?php echo ($test_info->current > $i) ? 'val' : (($test_info->current == $i) ? 'active' : ''); ?>"><i class="icon"></i>Soal <?php echo $i ?></li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>