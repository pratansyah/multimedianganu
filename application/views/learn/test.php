
<section id="quizz-intro-section" class="quizz-intro-section learn-section">
    <div class="container">


        <div class="question-content-wrap">
            <div class="row">
                <div class="col-md-8">
                    <div class="question-content">
                        <h4 class="md">Soal <?php echo $test_info->current; ?></h4>
                        <p><?php echo $question->question ?></p>
                        <form method="POST" action="<?php echo intval($test_info->sequence)+1; ?>">
                        <input type="hidden" name="question_id" value="<?php echo $question->question_id; ?>"/>
                            <div class="answer">
                                <h4 class="sm">Jawaban</h4>
                                <ul class="answer-list">
                                    <?php foreach($answers as $answer): ?>
                                    <li>
                                        <input type="radio" name="answer" value="<?php echo $answer->header; ?>" id="answer-<?php echo $answer->header; ?>">
                                        <label for="answer-<?php echo $answer->header; ?>">
                                            <i class="icon icon_radio"></i>
                                            <?php echo $answer->content; ?>
                                        </label>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                                <input type="hidden" name="time_left" id="time_left"/>
                                <input type="submit" value="Jawab" class="mc-btn btn-style-1">
                            </div>
                        </form>

                    </div>

                    
                </div>


                <div class="col-md-4">
                    <aside class="question-sidebar">
                        <div class="score-sb">
                                <h4 class="title-sb sm bold">Sisa waktu<span></span></h4>
                            <ul>
                                <?php for($i = 1; $i <= $test_info->questions_number; $i++): ?>
                                    <li class="<?php echo ($test_info->current > $i) ? 'val' : (($test_info->current == $i) ? 'active' : ''); ?>"><i class="icon"></i>Soal <?php echo $i ?></li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var time_left = 0
    function startTimer(duration, display) {
        var start = Date.now(),
            diff,
            minutes,
            seconds;
        function timer() {
            // get the number of seconds that have elapsed since 
            // startTimer() was called
            diff = duration - (((Date.now() - start) / 1000) | 0);

            // does the same job as parseInt truncates the float
            minutes = (diff / 60) | 0;
            seconds = (diff % 60) | 0;

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;
            time_left = diff;

            if (diff <= 0) {
                // add one second so that the count down starts at the full duration
                // example 05:00 not 04:59
                start = Date.now() + 1000;
            }
        };
        // don't wait a full second before the timer starts
        timer();
        setInterval(timer, 1000);
    }

    window.onload = function () {
        var fiveMinutes = 60 * 5,
        <?php if(isset($time_left)) echo "time_left=".$time_left.";"; else echo "time_left = window.name;"?>

        display = document.querySelector('.title-sb span');
        startTimer(time_left, display);
    }
    window.onunload = function() {
        window.name = time_left;
    }
</script>