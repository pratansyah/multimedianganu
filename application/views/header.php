<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <!-- Google font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/library/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/library/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/library/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/md-font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo assets_url(); ?>css/style.css">

    <!-- Include Editor style. -->
    <link href="<?php echo assets_url(); ?>js/library/froala/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo assets_url(); ?>js/library/froala/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery-1.11.0.min.js"></script>
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <title>Multimedia Nganu</title>
</head>
<body id="page-top" class="home">

<!-- PAGE WRAP -->
<div id="page-wrap">

    <!-- PRELOADER -->
    <div id="preloader">
        <div class="pre-icon">
            <div class="pre-item pre-item-1"></div>
            <div class="pre-item pre-item-2"></div>
            <div class="pre-item pre-item-3"></div>
            <div class="pre-item pre-item-4"></div>
        </div>
    </div>
    <!-- END / PRELOADER -->

    <!-- HEADER -->
    <header id="header" class="header">
        <div class="container">

            <!-- LOGO -->
            <!-- <div class="logo"><a href="index.html"><img src="images/logo.png" alt=""></a></div> -->
            <!-- END / LOGO -->

            <!-- NAVIGATION -->
            <nav class="navigation">

                <div class="open-menu">
                    <span class="item item-1"></span>
                    <span class="item item-2"></span>
                    <span class="item item-3"></span>
                </div>

                <!-- MENU -->
                <ul class="menu">
                    <li class="current-menu-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                    <?php if($this->session->userdata('admin')): ?>
                    <li>
                        <a href="<?php echo base_url().'admin/course'; ?>">Materi</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'admin/student'; ?>">Siswa</a>
                    </li>
                    <?php endif; ?>
                    <?php if(!$this->session->userdata('student') || !$this->session->userdata('admin')): ?>
                    <li class="menu-item">
                        <a href="<?php echo base_url().'login'; ?>">Login</a>
                    </li>
                    <?php endif; ?>
                    <!-- <li>
                        <a href="categories.html">Course</a>
                    </li> -->
                </ul>
                <!-- END / MENU -->
                <?php if($this->session->userdata('student')): $student = $this->session->userdata('student')?>
                <!-- LIST ACCOUNT INFO -->
                <ul class="list-account-info">

                    <li class="list-item account">
                        <div class="account-info item-click">
                            <img src="<?php echo assets_url(); ?>images/profile/<?php echo $student->image; ?>" alt="">
                        </div>
                        <div class="toggle-account toggle-list">
                            <ul class="list-account">
                                <li><a href="<?php echo base_url().'user' ?>"><i class="icon fa fa-user"></i>Profile</a></li>
                                <li><a href="<?php echo base_url().'logout' ?>"><i class="icon md-arrow-right"></i>Log out</a></li>
                            </ul>
                        </div>
                    </li>

                </ul>
                <!-- END / LIST ACCOUNT INFO -->
                <?php endif; ?>

            </nav>
            <!-- END / NAVIGATION -->

        </div>
    </header>
    <!-- END / HEADER -->