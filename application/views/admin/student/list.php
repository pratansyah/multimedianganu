<!-- COURSE CONCERN -->
<section id="course-concern" class="course-concern">
    <div class="container">
        
        <div class="table-asignment">


            <!-- Tab panes -->
            <div class="tab-content">
                <!-- MY SUBMISSIONS -->
                <div class="tab-pane fade in active" id="mysubmissions">
                    <div class="table-wrap">

                        <!-- TABLE BODY -->
                        <div class="table-body">
                            <!-- TABLE ITEM -->
                            <?php if(isset($students) && $students !== false): foreach($students as $student_id => $student): if(is_array($student)):?>
                            <div class="table-item">
                                <div class="thead">
                                    <div class="submissions" style="width: 25%"><?php echo $students_name[$student_id]; ?></a></div>
                                    <div class="total-subm">Pre-test</div>
                                    <div class="replied">Latihan 1</div>
                                    <div class="latest-reply">Latihan 2</div>
                                    <div class="latest-reply">Post-test</div>
                                    <div class="toggle tb-icon">
                                        <a href="#"><i class="fa fa-angle-down"></i></a>
                                    </div>
                                </div>
                                <?php foreach($student as $course_name => $course): ?>
                                <div class="tbody">
                                    <!-- ITEM -->
                                    <div class="item">
                                        <div class="submissions" style="width: 25%"><a href="#"><?php echo $course_name ?></a></div>
                                        <div class="total-subm"><?php echo isset($course[1]) ? $course[1]->score.'/'.$course[1]->try : '0/0' ?></div>
                                        <div class="replied"><?php echo isset($course[2]) ? $course[2]->score.'/'.$course[2]->try : '0/0' ?></div>
                                        <div class="latest-reply"><?php echo isset($course[3]) ? $course[3]->score.'/'.$course[3]->try : '0/0' ?></div>
                                        <div class="latest-reply"><?php echo isset($course[4]) ? $course[4]->score.'/'.$course[4]->try : '0/0' ?></div>
                                        <div class="link tb-icon">
                                            <a href="#"><?php echo isset($course[4]) ? $course[4]->score : '0/0' ?></a>
                                        </div>
                                    </div>
                                    <!-- END / ITEM -->

                                </div>
                                <?php endforeach;?>
                            </div>
                            <?php endif; endforeach; endif; ?>
                            <!-- END / TABLE ITEM -->
                            
                        </div>
                        <!-- END / TABLE BODY -->
                    </div>

                </div>
                <!-- END / MY SUBMISSIONS -->

            </div>

        </div>
    </div>
</section>
<!-- END / COURSE CONCERN -->

<script type="text/javascript">

$.each($('.table-wrap'), function() {
    $(this)
        .find('.table-item')
        .children('.thead:not(.active)')
        .next('.tbody').hide();
    $(this)
        .find('.table-item')
        .delegate('.thead', 'click', function(evt) {
            evt.preventDefault();
            if ($(this).hasClass('active')==false) {
                $('.table-item')
                    .find('.thead')
                    .removeClass('active')
                    .siblings('.tbody')
                        .slideUp(200);
            }
            $(this)
                .toggleClass('active')
                .siblings('.tbody')
                    .slideToggle(200);
    });
});

</script>