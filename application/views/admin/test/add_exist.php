<!-- BANNER CREATE COURSE -->
<section class="sub-banner sub-banner-create-course">
    <div class="awe-color bg-color-1"></div>
    <div class="container">
        <h2 class="md ilbl">Tambah Soal</h2>
    </div>
</section>
<!-- END / BANNER CREATE COURSE -->

<!-- CREATE COURSE CONTENT -->
<section id="create-course-section" class="create-course-section" style="padding-bottom: 11%">
    <div class="container">
        <div class="row">

            <div class="col-md-10">
                <?php echo validation_errors(); ?>
                <form class="create-course-content" method="POST">

                    <div class="promo-video create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Tambah Durasi</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="form-item">
                                    <input type="text" placeholder="Durasi (dalam menit)" name="duration" value="<?php echo (set_value('duration')) ? set_value('duration') : 0; ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo-video create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Tambah Jumlah Soal</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="form-item">
                                    <input type="text" placeholder="Jumlah Soal" name="jumlah_soal" value="<?php echo set_value('jumlah_soal'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-action">
                        <input type="submit" value="Tambah" class="mc-btn-3 btn-style-1" />
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>
<!-- END / CREATE COURSE CONTENT -->
