<!-- BANNER CREATE COURSE -->
<section class="sub-banner sub-banner-create-course">
    <div class="awe-color bg-color-1"></div>
    <div class="container">
        <h2 class="md ilbl">Perbarui Soal</h2>
    </div>
</section>
<!-- END / BANNER CREATE COURSE -->

<!-- CREATE COURSE CONTENT -->
<section id="create-course-section" class="create-course-section" style="padding-bottom: 11%">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <?php echo validation_errors(); ?>
                <form class="create-course-content" method="POST">

                    <div class="description create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Soal nomor <?php echo $sequence; ?></h4>
                            </div>
                            <div class="col-md-9">
                                <div class="description-editor text-form-editor">
                                    <textarea placeholder="Pertanyaan" name="question" class="froala-box"><?php echo (set_value('question')) ? set_value('question') : $question->question; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo-video create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Jawaban</h4>
                            </div>
                            <div class="col-md-1">
                                A: 
                            </div>
                            <div class="col-md-8">
                                <div class="form-item">
                                    <input type="text" placeholder="" name="a" value="<?php echo (set_value('a')) ? set_value('a') : $answer['a']; ?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <h4></h4>
                            </div>
                            <div class="col-md-1">
                                B: 
                            </div>
                            <div class="col-md-8">
                                <div class="form-item">
                                    <input type="text" placeholder="" name="b" value="<?php echo (set_value('b')) ? set_value('b') : $answer['b']; ?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <h4></h4>
                            </div>
                            <div class="col-md-1">
                                C: 
                            </div>
                            <div class="col-md-8">
                                <div class="form-item">
                                    <input type="text" placeholder="" name="c" value="<?php echo (set_value('c')) ? set_value('c') : $answer['c']; ?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <h4></h4>
                            </div>
                            <div class="col-md-1">
                                D: 
                            </div>
                            <div class="col-md-8">
                                <div class="form-item">
                                    <input type="text" placeholder="" name="d" value="<?php echo (set_value('d')) ? set_value('d') : $answer['d']; ?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <h4></h4>
                            </div>
                            <div class="col-md-1">
                                E: 
                            </div>
                            <div class="col-md-8">
                                <div class="form-item">
                                    <input type="text" placeholder="" name="e" value="<?php echo (set_value('e')) ? set_value('e') : $answer['e']; ?>">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="promo-video create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Kunci</h4>
                            </div>
                            <div class="form-item mc-select col-md-1">
                                <select class="select" name="key">
                                    <option value="a" <?php if($question->key_answer == 'a') echo 'selected'; ?>>A</option>
                                    <option value="b" <?php if($question->key_answer == 'b') echo 'selected'; ?>>B</option>
                                    <option value="c" <?php if($question->key_answer == 'c') echo 'selected'; ?>>C</option>
                                    <option value="d" <?php if($question->key_answer == 'd') echo 'selected'; ?>>D</option>
                                    <option value="e" <?php if($question->key_answer == 'e') echo 'selected'; ?>>E</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="description create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Catatan</h4>
                            </div>
                            <div class="col-md-9">
                                <div class="description-editor text-form-editor">
                                    <textarea placeholder="Pertanyaan" name="note" class="froala-box"><?php echo (set_value('note')) ? set_value('note') : $question->note; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-action">
                        <input type="submit" value="Perbarui" class="mc-btn-3 btn-style-1" />
                    </div>
                    
                </form>
            </div>
			<div class="col-md-3">
                <div id="uploader" class="create-course-content">
                    <div class="uploading upload-info text-center tb">
                        <div class="add-thumb-wrap tb-cell">
                            <a href="#" id="add-video">
                                <i class="icon md-plus"></i>
                                Tambah video
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </div>
</section>
<!-- END / CREATE COURSE CONTENT -->