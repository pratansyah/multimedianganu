<!-- BANNER CREATE COURSE -->
<section class="sub-banner sub-banner-create-course">
    <div class="awe-color bg-color-1"></div>
    <div class="container">
        <h2 class="md ilbl">Perbarui Materi</h2>
    </div>
</section>
<!-- END / BANNER CREATE COURSE -->

<!-- CREATE COURSE CONTENT -->
<section id="create-course-section" class="create-course-section" style="padding-bottom: 11%">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <?php echo validation_errors(); ?>
                <form class="create-course-content" method="POST">

                    <div class="promo-video create-item">
                        <div class="row">
                            <div class="col-md-2">
                                <h4>Pelajaran</h4>
                            </div>
                            <div class="col-md-10">
                                <?php if($course){ echo $course->name; }?>
                            </div>
                        </div>
                    </div>
                    <div class="promo-video create-item">
                        <div class="row">
                            <div class="col-md-2">
                                <h4>Nama materi</h4>
                            </div>
                            <div class="col-md-10">
                                <div class="form-item">
                                    <input type="text" placeholder="Nama materi" name="name" value="<?php echo $lesson->name; ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="description create-item">
                        <div class="row">
                            <div class="col-md-2">
                                <h4>Konten materi</h4>
                            </div>
                            <div class="col-md-10">
                                <div class="description-editor text-form-editor">
                                    <textarea placeholder="Konten" name="content" class="froala-box"><?php echo $lesson->content; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-action">
                        <input type="submit" value="Perbarui" class="mc-btn-3 btn-style-1" />
                    </div>
                    
                </form>
            </div>
            <div class="col-md-3">
                <div id="uploader" class="create-course-content">
                    <div class="uploading upload-info text-center tb">
                        <div class="add-thumb-wrap tb-cell">
                            <a href="#" id="add-video">
                                <i class="icon md-plus"></i>
                                Tambah video
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END / CREATE COURSE CONTENT -->
