<!-- BANNER CREATE COURSE -->
<section class="sub-banner sub-banner-create-course">
    <div class="awe-color bg-color-1"></div>
    <div class="container">
        <h2 class="md ilbl">Tambah Pelajaran</h2>
    </div>
</section>
<!-- END / BANNER CREATE COURSE -->

<!-- CREATE COURSE CONTENT -->
<section id="create-course-section" class="create-course-section" style="padding-bottom: 11%">
    <div class="container">
        <div class="row">

            <div class="col-md-10">
                <?php echo validation_errors(); ?>
                <form class="create-course-content" method="POST">

                    <!-- PROMO VIDEO -->
                    <div class="promo-video create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Nama pelajaran</h4>
                            </div>
                            <div class="col-md-9">
                                <div class="form-item">
                                    <input type="text" placeholder="Nama pelajaran" name="name" value="<?php echo set_value('name'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / PROMO VIDEO -->

                    <!-- DESCRIPTION -->
                    <div class="description create-item">
                        <div class="row">
                            <div class="col-md-3">
                                <h4>Deskripsi singkat</h4>
                            </div>
                            <div class="col-md-9">
                                <div class="description-editor text-form-editor">
                                    <textarea placeholder="Deskripsi singkat" name="description" class="froala-box"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / DESCRIPTION -->

                    <div class="form-action">
                        <input type="submit" value="Tambah" class="mc-btn-3 btn-style-1" />
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>
<!-- END / CREATE COURSE CONTENT -->