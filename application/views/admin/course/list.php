
<section class="sub-banner sub-banner-create-course">
    <div class="awe-color bg-color-1"></div>
    <div class="container">
        <h2 class="md ilbl">Daftar Pelajaran</h2>
    </div>
</section>
<section id="quizz-intro-section" class="quizz-intro-section learn-section" style="padding: 0px;">
    <div class="container">

        <a href="<?php echo base_url().'admin/course/add' ?>" class="mc-btn btn-style-1">Tambah</a>

        <div class="table-student-submission">
            <table class="mc-table">
                <thead>
                    <tr>
                        <th class="submissions">Nama Pelajaran</th>
                        <th class="author">Deskripsi <span class="caret"></span></th>
                        <th class="submit-date">Tanggal dibuat <span class="caret"></span></th>
                    </tr>
                </thead>

                <tbody>
                <?php if($courses): foreach($courses as $course): ?>
                    <tr>
                        <td class="submissions"><a href="<?php echo base_url().'admin/course/detail/'.$course->course_id; ?>"><?php echo $course->name ?></a></td>
                        <td class="author"><?php echo excerpt($course->description); ?></td>
                        <td class="submit-date"><?php echo convert_date($course->created); ?></td>
                    </tr>
                <?php endforeach;endif; ?>
                </tbody>
            </table>
        </div>

        
    </div>
</section>