<section class="sub-banner sub-banner-create-course">
    <div class="awe-color bg-color-1"></div>
    <div class="container">
        <h2 class="md ilbl"><?php echo $course->name ?></h2>
    </div>
</section>
<!-- COURSE -->
<section class="course-top">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="sidebar-course-intro" style="padding-top: 75px;">
                    <div class="about-instructor">
                        <p><?php echo $course->description; ?></p>
                    </div>
                </div>
            </div>    
            <div class="col-md-7">
                <div class="tabs-page">
                    <ul class="nav-tabs" role="tablist">
                        <li class="active"><a href="course-learn.html#outline" role="tab" data-toggle="tab">Tes</a></li>
                        <li class=""><a href="course-learn.html#announcement" role="tab" data-toggle="tab">Materi</a></li>
                        <!-- <li><a href="course-learn.html#student" role="tab" data-toggle="tab">Student</a></li> -->
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <!-- TESTS -->
                        <div class="tab-pane fade in active" id="outline">

                            <!-- SECTION TEST -->
                            <div class="section-outline">
                                <h4 class="tit-section xsm"></h4>
                                <ul class="section-list">
                                    <?php for($sequence = 1; $sequence <=4; $sequence++): ?>
                                        <?php if(isset($tests[$sequence])): ?>
                                        <li class="o-view">
                                            <div class="count"><span><?php echo $sequence; ?></span></div>
                                            <div class="list-body">
                                                <h2><?php echo $tests_key[$sequence]; ?></h2>
                                                <div class="data-lessons">
                                                    <?php
                                                        echo "<span>Jumlah: ".$tests[$sequence]->questions_number." Soal</span></br>";
                                                        echo "<span>Waktu: ".$tests[$sequence]->time_allocation." Menit</span></br>";
                                                        echo "<span>Nilai Minimal: ".$tests[$sequence]->passing_grade."</span>";
                                                    ?>
                                                </div>

                                                <div class="download">
                                                    <a href="<?php echo base_url().'admin/test/edit/'.$tests[$sequence]->test_id; ?>"><i class="icon md-pencil"></i></a>
                                                    <div class="download-ct">
                                                        <span>Perbarui Soal</span>
                                                    </div>
                                                <div class="download">
                                                    <a href="<?php echo base_url().'admin/test/add_exist/'.$course->course_id.'/'.$sequence.'/multiple'; ?>"><i class="icon md-plus"></i></a>
                                                    <div class="download-ct">
                                                        <span>Tambah Soal</span>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="div-x"><i class="icon md-check-2"></i></div>
                                                <div class="line"></div>
                                            </div>
                                        </li>
                                        <?php else: ?>
                                        <li>
                                            <div class="count"><span><?php echo $sequence; ?></span></div>
                                            <div class="list-body">
                                                <h2><?php echo $tests_key[$sequence]; ?></h2>

                                                <div class="download">
                                                    <a href="<?php echo base_url().'admin/test/add/'.$course->course_id.'/'.$sequence.'/multiple'; ?>"><i class="icon md-plus"></i></a>
                                                    <div class="download-ct">
                                                        <span>Tambah Soal</span>
                                                    </div>
                                                </div>
                                                <div class="div-x"><i class="icon md-check-2"></i></div>
                                                <div class="line"></div>
                                            </div>
                                        </li>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </ul>
                            </div>
                            <!-- END / SECTION TEST -->

                        </div>
                        <!-- END / TESTS -->

                        <!-- LESSONS -->
                        <div class="tab-pane fade" id="announcement">
                        <a href="<?php echo base_url().'admin/lesson/add/'.$course->course_id ?>" class="mc-btn btn-style-1">Tambah</a>
                            <?php if($lessons): ?>
                            <ul class="list-announcement">
                                <?php foreach($lessons as $lesson): ?>
                                <!-- LIST ITEM -->
                                <li>
                                    <div class="list-body">
                                        <i class="icon md-flag"></i>
                                        <div class="list-content">
                                            <h4 class="sm black bold">
                                                <a href="<?php echo base_url().'learn/lesson/'.$lesson->lesson_id; ?>"><?php echo $lesson->name ?></a>
                                            </h4>
                                            <?php echo excerpt($lesson->content, 250); ?>
                                            <div class="author"><a href="<?php echo base_url().'admin/lesson/edit/'.$lesson->lesson_id ?>">Edit</a></div>
                                            <!-- <em>5 days ago</em> -->
                                        </div>
                                    </div>
                                </li>
                                <!-- END / LIST ITEM -->
                                <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>
                        </div>
                        <!-- END / LESSONS -->

                        <!-- STUDENT -->
                        <div class="tab-pane fade" id="student">
                            <h3 class="md black">53618 Students applied</h3>
                            <div class="tab-list-student">
                                <ul class="list-student">
                                    <!-- LIST STUDENT -->
                                    <li>
                                        <div class="image">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <div class="list-body">
                                            <cite class="xsm"><a href="course-learn.html#">Neo Khuat</a></cite>
                                            <span class="address">Hanoi, Vietnam</span>
                                            <div class="icon-wrap">
                                                <a href="course-learn.html#"><i class="icon md-email"></i></a>
                                                <a href="course-learn.html#"><i class="icon md-user-plus"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END / LIST STUDENT -->

                                    <!-- LIST STUDENT -->
                                    <li>
                                        <div class="image">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <div class="list-body">
                                            <cite class="xsm"><a href="course-learn.html#">Neo Khuat</a></cite>
                                            <span class="address">Hanoi, Vietnam</span>
                                            <div class="icon-wrap">
                                                <a href="course-learn.html#"><i class="icon md-email"></i></a>
                                                <a href="course-learn.html#"><i class="icon md-user-plus"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END / LIST STUDENT -->

                                    <!-- LIST STUDENT -->
                                    <li>
                                        <div class="image">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <div class="list-body">
                                            <cite class="xsm"><a href="course-learn.html#">Neo Khuat</a></cite>
                                            <span class="address">Hanoi, Vietnam</span>
                                            <div class="icon-wrap">
                                                <a href="course-learn.html#"><i class="icon md-email"></i></a>
                                                <a href="course-learn.html#"><i class="icon md-user-plus"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END / LIST STUDENT -->

                                    <!-- LIST STUDENT -->
                                    <li>
                                        <div class="image">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <div class="list-body">
                                            <cite class="xsm"><a href="course-learn.html#">Neo Khuat</a></cite>
                                            <span class="address">Hanoi, Vietnam</span>
                                            <div class="icon-wrap">
                                                <a href="course-learn.html#"><i class="icon md-email"></i></a>
                                                <a href="course-learn.html#"><i class="icon md-user-plus"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END / LIST STUDENT -->

                                    <!-- LIST STUDENT -->
                                    <li>
                                        <div class="image">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <div class="list-body">
                                            <cite class="xsm"><a href="course-learn.html#">Neo Khuat</a></cite>
                                            <span class="address">Hanoi, Vietnam</span>
                                            <div class="icon-wrap">
                                                <a href="course-learn.html#"><i class="icon md-email"></i></a>
                                                <a href="course-learn.html#"><i class="icon md-user-plus"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END / LIST STUDENT -->

                                    <!-- LIST STUDENT -->
                                    <li>
                                        <div class="image">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <div class="list-body">
                                            <cite class="xsm"><a href="course-learn.html#">Neo Khuat</a></cite>
                                            <span class="address">Hanoi, Vietnam</span>
                                            <div class="icon-wrap">
                                                <a href="course-learn.html#"><i class="icon md-email"></i></a>
                                                <a href="course-learn.html#"><i class="icon md-user-plus"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END / LIST STUDENT -->

                                </ul>
                            </div>
                            <div class="load-more">
                                <a href="course-learn.html">
                                <i class="icon md-time"></i>
                                Load more previous update</a>
                            </div>
                        </div>
                        <!-- END / STUDENT -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END / COURSE TOP -->
