<!-- FOOTER -->
    <footer id="footer" class="footer">

        <div class="second-footer">
            <div class="container">
                <div class="contact">
                    <div class="email">
                        <i class="icon md-email"></i>
                        <a href="index.html#">edhardianti@gmail.com</a>
                    </div>
                    <div class="phone">
                        <i class="fa fa-mobile"></i>
                        <span>+6285722622141</span>
                    </div>
                    <div class="address">
                        <i class="fa fa-map-marker"></i>
                        <span>Bandung, ID</span>
                    </div>
                </div>
                <p class="copyright">Copyright © 2015 Elis Dwi Hardianti - 1002369</p>
            </div>
        </div>
    </footer>
    <!-- END / FOOTER -->

</div>
<!-- END / PAGE WRAP -->

<!-- Load jQuery -->
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery.owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery.appear.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/scripts.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/froala/js/froala_editor.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/froala/js/plugins/video.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/froala/js/plugins/tables.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/library/plupload/plupload.full.min.js"></script>

<script>
    $(function() {
        var pluploadProgress;
        $('.froala-box').editable({
            inlineMode: false,
            buttons: ['bold', 'italic', 'sep', 'formatBlock', 'indent', 'outdent',
                'insertOrderedList', 'insertUnorderedList', 'sep',
                'createLink', 'insertHorizontalRule', 'table', 'insertImage', 'insertVideo', 'subscript', 'superscript', 'sep', 'undo', 'redo'],

            // Set the image upload parameter.
            imageUploadParam: 'froala_image',

            // Set the image upload URL.
            imageUploadURL: "<?php echo base_url().'upload/froala'; ?>",

            // Additional upload params.
            imageUploadParams: {id: 'my_editor'}
        });

        var uploader = new plupload.Uploader({
            browse_button: 'add-video', // this can be an id of a DOM element or the DOM element itself
            url: "<?php echo base_url().'upload/plupload'; ?>"
        });
 
        uploader.init();

        uploader.bind('FilesAdded', function(up, files) {
            var html = '';
            plupload.each(files, function(file) {
                html += '<li id="' + file.id + '"><div style="word-wrap: break-word;">' + file.name + ' (' + plupload.formatSize(file.size) + ') </div><b></b></li>';
            });
            $('.uploading').after(html);
            uploader.start();
            // document.getElementById('filelist').innerHTML += html;
        });
        uploader.bind('UploadProgress', function(up, file) {
            $('#'+file.id+' b').html('<span>' + file.percent + "%</span>");
            // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
        });
        uploader.bind('FileUploaded', function(up, file, response){
            source = JSON.parse(response.response);
            $('#'+file.id+' b').after('</br>Embed code:</br><textarea onclick="this.select();"><video id="example_video_1" class="video-js vjs-default-skin" controls preload="auto" width="50%" data-setup=\'{"example_option":true}\'> <source src="'+source.link+'" type="video/mp4" /> <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> </video></textarea>');
            // console.log(JSON.parse(response.response));
        });

    });
</script>
</body>
</html>