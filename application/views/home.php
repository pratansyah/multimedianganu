<!-- HOME SLIDER -->
<section class="slide" style="background-image: url(<?php echo assets_url(); ?>images/homeslider/bg.jpg)">
    <div class="container">
        <div class="slide-cn" id="slide-home">
            <!-- SLIDE ITEM -->
            <div class="slide-item">
                <div class="item-inner">
                    <div class="text">
                        <h2>Curiousity is the key.</h2>
                        <p>Dont give up. Just dont.
                        </p>
                    </div>

                    <div class="img">
                        <img src="<?php echo assets_url(); ?>images/homeslider/x.png" alt="">
                    </div>
                </div>

            </div>  
            <!-- SLIDE ITEM -->     

            <!-- SLIDE ITEM -->
            <div class="slide-item">
                <div class="item-inner">
                    <div class="text">
                        <h2>Stay Hungry. Stay Foolish.</h2>
                        <p>Be ready to try new things
                        </p>
                    </div>

                    <div class="img">
                        <img src="<?php echo assets_url(); ?>images/homeslider/y.png" alt="">
                    </div>

                </div>  
            </div>  
            <!-- SLIDE ITEM -->  

        </div>
    </div>
</section>
<!-- END / HOME SLIDER -->

<!-- SECTION 1 -->
<section id="mc-section-1" class="mc-section-1 section">
    <div class="container">
        <div class="row">
            
            <div class="col-md-5">
                <div class="mc-section-1-content-1"> 
                    <h2 class="big">Overview</h2>
                    <p class="mc-text">
Modul ini dimaksudkan sebagai pegangan kegiatan belajar mengajar di SMK, khususnya untuk kompetensi Arsitektur dan Organisasi Komputer. Pada modul ini diuraikan pengertian dan perbedaan arsitektur dan oragisasi komputer, sejarah perkembangan komputer, hingga hirarki memori di komputer. Modul ini juga dilengkapi dengan beberapa contoh di setiap bagian. Modul ini dipersiapkan untuk memudahkan peserta didik dalam mempelajari Dengan mempelajari materi dalam modul ini, diharapkan peserta didik dapat menerapkan pengetahuan serta keterampilan mengenai Arsitektur dan Organisasi Kompute.</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-offset-1">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="featured-item">
                            <h4 class="title-box text-uppercase">Modul 1</h4>
                            <p>Pengantar Organisasi dan Arsitektur Komputer</p>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="featured-item">
                            <h4 class="title-box text-uppercase">Modul 2</h4>
                            <p>Media Penyimpan Data Eksternal</p>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="featured-item">
                            <h4 class="title-box text-uppercase">Modul 3</h4>
                            <p>Karakteristik Memory</p>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="featured-item">
                            <h4 class="title-box text-uppercase">Modul 4</h4>
                            <p>Memori Semikonduktor</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- END / SECTION 1 -->



<!-- SECTION 2 -->
<section id="mc-section-2" class="mc-section-2 section">
    <div class="awe-parallax bg-section1-demo"></div>
    <div class="overlay-color-1"></div>
    <div class="container">
        <div class="section-2-content">
            <div class="row">
                
                <div class="col-md-5">
                    <div class="ct">
                        <h2 class="big">Deskripsi singkat cara kerja</h2>
                        <p class="mc-text">
							<li>Sebelum memulai materi, pengguna terlebih dulu diharuskan untuk mengerjakan sejumlah soal sebagai pengukuran kemampuan awal </li>
							<li>Materi disajikan secara berurutan dari KD-1 ke KD-2</li>
							<li>Untuk bisa membuka materi di KD selanjutnya, pengguna harus menyelesaikan latihan di akhir materi dengan nilai minimal 78</li></p>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="image">
                        <img src="<?php echo assets_url(); ?>images/image.png" alt="">
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
<!-- END / SECTION 2 -->    