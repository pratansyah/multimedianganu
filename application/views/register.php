    <!-- LOGIN -->
    <section id="login-content" class="login-content">
        <div class="awe-parallax bg-login-content"></div>
        <div class="awe-overlay"></div>
        <div class="container">
            <div class="row">
    
    
                
                <!-- FORM -->
                <div class="col-lg-4 pull-right">
                    <div class="form-login">
                        <form method="POST">
                            <h2 class="text-uppercase">pendaftaran</h2>
                            <?php
                                if($this->session->flashdata('error')) echo "<p style='color: red;'>".$this->session->flashdata('error')."</p>";
                            ?>
                            <div class="form-datebirth">
                                <input class ="first-name"type="text" placeholder="Nomor Induk" name="nis">
                            </div>
                            <div class="form-datebirth">
                                <input class ="first-name"type="text" placeholder="Nama" name="name">
                            </div>
                            <div class="form-password">
                                <input type="password" placeholder="Password" name="password">
                            </div>
                            <div class="form-submit-1">
                                <input type="submit" value="Daftar" class="mc-btn btn-style-1">
                            </div>
                            <div class="link">
                                <a href="<?php echo base_url().'login' ?>">
                                    <i class="icon md-arrow-right"></i>Sudah punya akun ? Log in
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END / FORM -->
    
                <div class="image">
                    <img src="images/homeslider/img-thumb.png" alt="">
                </div>
    
            </div>
        </div>
    </section>
    <!-- END / LOGIN -->